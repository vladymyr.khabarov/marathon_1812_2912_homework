# My Learning Repository

This repository is part of the CyberBionic Systematics training marathon. It serves as a space for storing and
organizing my learning materials, exercises, and projects from the courses.

## Purpose

The main purpose of this repository is to:

- Document my progress during the training.
- Showcase the projects and exercises completed during the marathon.
- Provide a reference for future review and learning.

## Repository Owner

This repository is owned by Volodymyr Khabarov.

---

# marathon_1812_2912_homework

---

Марафон тривав два тижні та ми зустрічались 6 раз, тобто ви маєте 6 домашніх завдань. Фактично - це і є те саме шосте завдання.
Тепер Вам необхідно впорядкувати всі завдання для перевірки.

Для цього:
- зробіть форк вій цього репозиторію
- налаштуйте у своєму репозиторії все (мова про лінтери, особливості .gitignore - якщо вони для Вас є) як описано в завданні 2
- оновіть головну гілку
- додайте до проєкту всіх викладачів (контакти будуть у нашому чаті)
- заповніть README.md - там повинен бути опис роботи, хто виконував, особливості роботи якщо вони присутні і Ви вважаєте що той хто працює з Вашим репозиторієм повинен їх знати.
- додайте до проекту 
- виконайте домашні завдання (окрім другого, його виконувати не треба) - КОЖНЕ В ОКРЕМІЙ ГІЛЦІ яка має назву "homework_1", "homework_3", ..., відповідно.
- файл(и) з виконаним ДЗ повинен знаходитись у відповіній папці ("day_1", "day_3", ... - поряд з *.md файлом з текстом завдання)

Посилання саме на цей репозиторій внесіть у форму разом з іншими даними для оцінювання, посилання на яку з'явиться в нашому чаті марафону після закінчення останньої зустрічі.
